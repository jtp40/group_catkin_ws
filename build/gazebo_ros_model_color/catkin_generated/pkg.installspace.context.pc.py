# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;gazebo_ros".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lgazebo_ros_model_color".split(';') if "-lgazebo_ros_model_color" != "" else []
PROJECT_NAME = "gazebo_ros_model_color"
PROJECT_SPACE_DIR = "/home/jackson/group_catkin_ws/install"
PROJECT_VERSION = "0.0.0"
